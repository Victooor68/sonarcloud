/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.utils.test;

import java.math.BigDecimal;
import java.util.Arrays;

import th.co.cana.utils.Convertors;

/**
* @author supot
* @version 1.0
*/
public class TestConvertors {

	public static void main(String[] args) {
		System.out.println(Double.MAX_VALUE);
		System.out.println(Convertors.convert("1", Boolean.class));
		System.out.println(Convertors.convert(Integer.MAX_VALUE, Boolean.class));
		System.out.println(Convertors.convert(BigDecimal.ZERO, boolean.class));
		
		System.out.println(Convertors.convert("1", String.class));
		System.out.println(Convertors.convert(TestConvertors.class, String.class));
		
		System.out.println(Convertors.convert("1", int.class));
		System.out.println(Convertors.convert("1", Integer.class));
		
		System.out.println(Convertors.convert("1", long.class));
		System.out.println(Convertors.convert("", Long.class, 3L));
		
		byte[] datas = Convertors.convert("1234", byte[].class);
		System.out.println(Arrays.toString(datas));
		System.out.println(Convertors.convert("1234".getBytes(), String.class));
	}

}
