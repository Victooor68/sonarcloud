/*  ---------------------------------------------------------------------------
 *  * Copyright 2020-2021 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  ---------------------------------------------------------------------------
 */
package th.co.cana.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Utility class for manage date
 * @author Supot Saelao
 * @version 1.0
 */
public final class DateUtils {
	public static final Locale US = Locale.US;
	public static final Locale TH = new Locale("th", "TH");
	public static final int THAI_DIFF_YEAR 		= 543;
	public static final int TOTAL_WEEK_IN_MONTH = 7;
	
	public static final String FM_DATE 			= "dd/MM/yyyy";
	public static final String FM_DATE_TIME 	= "dd/MM/yyyy HH:mm:ss";
	public static final String FM_TIME 			= "HH:mm:ss";
	public static final String FM_NAME_SHORT 	= "dd MMM yyyy";
    public static final String FM_NAME_FULL		= "dd MMMM yyyy";
    
	private static final Map<String, Locale> LANGS;	
	static {
		LANGS = new HashMap<>(5);
		LANGS.put("en_US", US);
		LANGS.put("en_GB", Locale.UK);
		LANGS.put("ja_JP", Locale.JAPAN);
		LANGS.put("zh_CN", Locale.CHINA);
		LANGS.put("th_TH", TH);
	}
	
	private DateUtils() {
	}

	public static Locale getLocale(String key) {
		if (Validators.isEmpty(key)) {
			return US;
		}
		
		return LANGS.get(key);
	}
	
	public static Date date(Object value) {
		return DateFormats.date(value);
	}

	public static Date date(Object value, String pattern) {
		return DateFormats.date(value, pattern);
	}

	public static Date date(Object value, Locale locale) {
		return DateFormats.date(value, locale);
	}

	public static Date date(Object value, String pattern, Locale locale) {
		return DateFormats.date(value, pattern, locale);
	}

	public static LocalDate toLocalDate(String value) {
		return DateFormats.localDate(value);
	}
	
	public static LocalDate toLocalDate(String value, String pattern) {
		return DateFormats.localDate(value, pattern);
	}
	
	public static java.sql.Date getSqlDate() {
		return new java.sql.Date(new Date().getTime());
	}

	public static java.sql.Timestamp getSqlTimestamp() {
		return new java.sql.Timestamp(new Date().getTime());
	}

	public static java.sql.Date sqlDate(Object value) {
		return sqlDate(value, FM_DATE, US);
	}

	public static java.sql.Date sqlDate(Object value, Locale locale) {
		return sqlDate(value, FM_DATE, locale);
	}

	public static java.sql.Date sqlDate(Object value, String pattern) {
		return sqlDate(value, pattern, US);
	}

	public static java.sql.Date sqlDate(Object value, String pattern, Locale locale) {
		Date date = date(value, pattern, locale);
		if (date != null) {
			return new java.sql.Date(date.getTime());
		}
		
		return null;
	}

	public static java.sql.Timestamp sqlTimestamp(Object value) {
		return sqlTimestamp(value, FM_DATE_TIME, US);
	}

	public static java.sql.Timestamp sqlTimestamp(Object value, Locale locale) {
		return sqlTimestamp(value, FM_DATE_TIME, locale);
	}

	public static java.sql.Timestamp sqlTimestamp(Object value, String pattern) {
		return sqlTimestamp(value, pattern, US);
	}

	public static java.sql.Timestamp sqlTimestamp(Object value, String pattern, Locale locale) {
		Date date = date(value, pattern, locale);
		if (date != null) {
			return new java.sql.Timestamp(date.getTime());
		}
		return null;
	}

	public static String format(Date date) {
		return DateFormats.format(date);
	}

	public static String format(Date date, Locale locale) {
		return DateFormats.format(date, FM_DATE, locale);
	}
	
	public static String format(Date date, String pattern) {
		return DateFormats.format(date, pattern);
	}

	public static String format(Date date, String pattern, Locale locale) {
		return DateFormats.format(date, pattern, locale);
	}

	public static String format(LocalDate date) {
		return DateFormats.format(date);
	}
	
	public static String format(LocalDate date, String pattern) {
		return DateFormats.format(date, pattern);
	}
	
	public static String format(LocalDate date, String pattern, Locale locale) {
		return DateFormats.format(date, pattern, locale);
	}
	
	public static String format(LocalDateTime date) {
		return DateFormats.format(date);
	}
	
	public static String format(LocalDateTime date, String pattern) {
		return DateFormats.format(date, pattern);
	}
	
	public static String format(LocalDateTime date, String pattern, Locale locale) {
		return DateFormats.format(date, pattern, locale);
	}
	
	public static String formatThaiFull(Date date) {
		return DateFormats.format(date, FM_NAME_FULL, TH);
	}

	public static String formatThai(Date date) {
		return DateFormats.format(date, FM_NAME_SHORT, TH);
	}

	public static String formatThai(Date date, String pattern) {
		return DateFormats.format(date, pattern, TH);
	}

	public static Date getDate() {
		return new Date();
	}

	public static int getYear() {
		return Calendar.getInstance(US).get(Calendar.YEAR);
	}

	public static int getYearThai() {
		return getYearThai(getDate());
	}

	public static int getYear(Date date) {
		if (Validators.isNull(date)) {
			return 0;
		}

		Calendar cal = Calendar.getInstance(US);
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	public static int getYearThai(Date date) {
		if (Validators.isNull(date)) {
			return 0;
		}

		Calendar cal = Calendar.getInstance(TH);
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}

	public static int getMonth() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.MONTH) + 1;
	}

	public static int getMonth(Date date) {
		if (date == null) {
			return 0;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal.get(Calendar.MONTH) + 1;
	}
	
	public static int getDayOfMonth() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public static int getDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public static int getTotalDayOfMonth() {
		return getTotalDayOfMonth(getDate());
	}
	
	public static int getTotalDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	public static int getWeekOfMonth() {
		return getWeekOfMonth(getDate());
	}
	
	public static int getWeekOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		if (dayOfMonth <= TOTAL_WEEK_IN_MONTH) {
			return 1;
		} else if (dayOfMonth <= 14) {
			return 2;
		} else if (dayOfMonth <= 21) {
			return 3;
		} else {
			return 4;
		}
	}
	
	public static String getDayOfMonthName() {
		return getDayOfMonthName(getDate());
	}

	public static String getDayOfMonthName(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
	}

	public static Date addSecond(Date date, int second) {
		return addDate(date, Calendar.SECOND, second);
	}
	
	public static Date addMinute(Date date, int minute) {
		return addDate(date, Calendar.MINUTE, minute);
	}

	public static Date addHour(Date date, int hour) {
		return addDate(date, Calendar.HOUR, hour);
	}

	public static Date addDay(Date date, int day) {
		return addDate(date, Calendar.DATE, day);
	}

	public static Date addWeek(Date date, int week) {
		return addDate(date, Calendar.DATE, (week * 7));
	}
	
	public static Date addMonth(Date date, int month) {
		return addDate(date, Calendar.MONTH, month);
	}

	public static Date addYear(Date date, int year) {
		return addDate(date, Calendar.YEAR, year);
	}
	
	public static Date trunc(Date date) {
		if (Validators.isNull(date)) {
			return null;
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static Date max(Date date1, Date date2) {
		if (date1 == null) {
			return date2;
		}
		if (date2 == null) {
			return date1;
		}
		return (date1.after(date2)) ? date1 : date2;
	}

	public static Date min(Date date1, Date date2) {
		if (date1 == null) {
			return date1;
		}
		if (date2 == null) {
			return date2;
		}
		return (date1.before(date2)) ? date1 : date2;
	}
	
	private static Date addDate(Date date, int type, int value) {
		if (date == null || value == 0) {
			return date;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(type, value);
		return cal.getTime();
	}
}
