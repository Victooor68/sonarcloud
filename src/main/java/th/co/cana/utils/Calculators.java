/*  ---------------------------------------------------------------------------
 *  * Copyright 2020-2021 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  ---------------------------------------------------------------------------
 */
package th.co.cana.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
 

/**
 * Utility class for calculation data
 * @author Supot Saelao
 * @version 1.0
 */
public final class Calculators {
    private static final int SCALE_2  = 2;
    
    private Calculators() {}
    
    public static BigDecimal roundDown(BigDecimal value) {
        return divide(value, Values.ONE, RoundingMode.DOWN);
    } 
    
    public static BigDecimal roundDown(BigDecimal value, int scale) {
        return round(value, scale, RoundingMode.DOWN);
    }    
    
    public static BigDecimal roundUp(BigDecimal value) {
    	return divide(value, Values.ONE, RoundingMode.UP);
    }
    
    public static BigDecimal roundUp(BigDecimal value, int scale) {
        return round(value, scale, RoundingMode.UP);
    }
    
    /**
     * Rounding of value with half-up mode
     * @param value
     * @return 
     */
    public static BigDecimal round(BigDecimal value) {
    	return divide(value, Values.ONE, RoundingMode.HALF_UP);
    }
    
    /**
     * Rounding of value with half-up mode
     * @param value
     * @param scale
     * @return
     */
    public static BigDecimal round(BigDecimal value, int scale) {
        return round(value, scale, RoundingMode.HALF_UP);
    }
    
    public static BigDecimal round(BigDecimal value, int scale, RoundingMode mode) {
        if (Validators.isNull(value)) {
            return value;
        }
        
		if (scale < 0) {
        	return value;
        }
        
        return value.setScale(scale, mode);
    }

    /**
     * Calculate amount of percent from total amount
     * @param value The total of amount
     * @param percent The percent value
     * @return
     */
    public static BigDecimal amountOfPercent(BigDecimal value, BigDecimal percent) {
        return amountOfPercent(value, percent, SCALE_2, RoundingMode.HALF_UP);
    }

    /**
     * Calculate amount of percent from total amount
     * @param value The total of amount
     * @param percent The percent value
     * @param scale Rounding Scale
     * @return
     */
    public static BigDecimal amountOfPercent(BigDecimal value, BigDecimal percent, int scale) {
        return amountOfPercent(value, percent, scale, RoundingMode.HALF_UP);
    }
    
    /**
     * Calculate amount of percent from total amount
     * @param value The total of amount
     * @param percent The percent value
     * @param scale Rounding Scale
     * @param mode Rounding mode {@link RoundingMode}
     * @return
     */
    public static BigDecimal amountOfPercent(BigDecimal value, BigDecimal percent, int scale,
            RoundingMode mode) {
        
        if (Validators.isNull(value)) {
            value = BigDecimal.ZERO;
        }
        if (Validators.isNull(percent)) {
            percent = BigDecimal.ZERO;
        }
        value = value.multiply(percent);
        
        return divide(value, Values.ONE_HUNDRED, scale, mode);
    }
 
    /**
     * Calculate percent value from total amount
     * @param totalAmt The total amount
     * @param amt The amount for calculate percent
     * @return
     */
    public static BigDecimal percent(BigDecimal totalAmt, BigDecimal amt) {
    	return percent(totalAmt, amt, SCALE_2, RoundingMode.HALF_UP);
    }
    
    /**
     * Calculate percent value from total amount
     * @param totalAmt The total amount
     * @param amt The amount for calculate percent
     * @param scale Rounding scale
     * @return
     */
    public static BigDecimal percent(BigDecimal totalAmt, BigDecimal amt, int scale) {
    	return percent(totalAmt, amt, scale, RoundingMode.HALF_UP);
    }
    
    /**
     * Calculate percent value from total amount
     * @param totalAmt The total amount
     * @param amt The amount for calculate percent
     * @param scale Rounding scale
     * @param mode mode Rounding mode {@link RoundingMode}
     * @return
     */
    public static BigDecimal percent(BigDecimal totalAmt, BigDecimal amt, int scale,
            RoundingMode mode) {
        
        if (Validators.isNull(totalAmt) || Validators.isNull(amt)) {
            return BigDecimal.ZERO;
        }
		if (Validators.isZero(totalAmt)) {
			return BigDecimal.ZERO;
		}
		if (Validators.isNull(amt)) {
			amt = BigDecimal.ZERO;
		}
		amt = amt.multiply(Values.ONE_HUNDRED);
		return divide(amt, totalAmt, scale, mode);
	}
    
    public static BigDecimal add(BigDecimal value, Number add) {
        if (Validators.isNull(value) && Validators.isNull(add)) {
            return BigDecimal.ZERO;
		}
        
		value = (Validators.isNull(value) ? BigDecimal.ZERO : value);
		if (Validators.isNull(add)) {
			return value;
		}
 		
		BigDecimal addValue = Convertors.toBigDecimal(add);
        return value.add(addValue);
    }
    
    public static BigDecimal subtract(BigDecimal value, Number sub) {
        if (Validators.isNull(value) && Validators.isNull(sub)) {
            return BigDecimal.ZERO;
        } 
		
        value = (Validators.isNull(value) ? BigDecimal.ZERO : value);
		if (Validators.isNull(sub)) {
			return value;
		}
		
		BigDecimal addValue = Convertors.toBigDecimal(sub);
        return value.subtract(addValue);
    }
    
    /**
     * Returns a BigDecimal whose value is (value / divisor), and rounding must be half-up mode<br/>
     * @param value
     * @param divisor
     * @return The result of (value / divisor)
     */
    public static BigDecimal divide(BigDecimal value, Number divisor) {
        return divide(value, divisor, RoundingMode.HALF_UP);
    }

    public static BigDecimal divide(BigDecimal value, Number divisor, RoundingMode mode) {
		if (Validators.isNull(value) || Validators.isNull(divisor)) {
			return BigDecimal.ZERO;
		}

		try {
			BigDecimal divisorValue = Convertors.toBigDecimal(divisor);
			return value.divide(divisorValue, mode);
		} catch (ArithmeticException ex) {
			return BigDecimal.ZERO;
		}
    }
    
    /**
     * Returns a BigDecimal whose value is (value / divisor), and rounding must be half-up mode<br/>
     * @param value
     * @param divisor
     * @param scale The scale of rounding position
     * @return The result of (value / divisor) with set scale
     */
    public static BigDecimal divide(BigDecimal value, Number divisor, int scale) {
        return divide(value, divisor, scale, RoundingMode.HALF_UP);
    }

    public static BigDecimal divide(BigDecimal value, Number divisor, int scale, RoundingMode mode) {

		if (Validators.isNull(value) || Validators.isNull(divisor)) {
			return BigDecimal.ZERO;
		}

		try {
			BigDecimal divisorValue = Convertors.toBigDecimal(divisor);
			return value.divide(divisorValue, scale, mode);
		} catch (ArithmeticException ex) {
			return BigDecimal.ZERO;
		}
	}
    
    public static BigDecimal multiply(BigDecimal value, Number multiply) {
        if (Validators.isNull(value) && Validators.isNull(multiply)) {
            return BigDecimal.ZERO;
        } 
		
        value = (Validators.isNull(value) ? BigDecimal.ZERO : value);
		if (Validators.isNull(multiply)) {
			return value.multiply(BigDecimal.ZERO);
		}
		
		BigDecimal addValue = Convertors.toBigDecimal(multiply);
        return value.multiply(addValue);
    }
    
	public static BigDecimal negative(BigDecimal value) {
		if (Validators.isNull(value)) {
			return null;
		}

		return value.multiply(BigDecimal.valueOf(-1D));
	}

	public static BigDecimal negative(Double value) {
		if (Validators.isNull(value)) {
			return null;
		}

		return BigDecimal.valueOf(value).multiply(BigDecimal.valueOf(-1D));
	}
	
	public static BigDecimal abs(BigDecimal value) {
		if (Validators.isNull(value)) {
			return BigDecimal.ZERO;
		}
		
		return BigDecimal.valueOf(Math.abs(value.doubleValue()));
	}
	
	/**
	 * Calculate amount with no VAT
	 * @param amount The value for calculate
	 * @param vatRate The VAT rate
	 * @return
	 */
	public static VatAmount calculatNoVat(BigDecimal amount, BigDecimal vatRate) {
		VatAmount valAmount = new VatAmount();
		valAmount.setAmount(amount);
		valAmount.setVatRate(vatRate);
		if (Validators.isNull(amount)) {
			return valAmount;
		}

		valAmount.setVatExcludeAmt(amount);
		valAmount.setVatIncludeAmt(amount);

		return valAmount;
	}

	/**
	 * Calculate amount with including VAT
	 * @param amount The value for calculate
	 * @param vatRate The VAT rate
	 * @return <code>VatAmount</code> 
	 * @see th.co.cana.utils.Financials.VatAmount
	 */
	public static VatAmount calculateIncludeVat(BigDecimal amount, BigDecimal vatRate) {
		VatAmount valAmount = new VatAmount();
		valAmount.setAmount(amount);
		valAmount.setVatRate(vatRate);

		if (Validators.isNull(amount)) {
			return valAmount;
		}

		// Vat = (amount * vatRate)/(100 + vatRate)
		BigDecimal vatAmt = multiply(amount, vatRate);
		vatAmt = divide(vatAmt, Values.ONE_HUNDRED.add(vatRate));

		// Exclude vatAmt = (Include vatAmt-vateAmt)
		BigDecimal excVatAmt = subtract(amount, vatAmt);

		valAmount.setVatAmt(vatAmt);
		valAmount.setVatIncludeAmt(amount);
		valAmount.setVatExcludeAmt(excVatAmt);

		return valAmount;
	}

	/**
	 * Calculate amount with excluding VAT
	 * @param amount The value for calculate
	 * @param vatRate The VAT rate
	 * @return
	 * @see th.co.cana.utils.Financials.VatAmount
	 */
	public static VatAmount calculateExcludeVat(BigDecimal amount, BigDecimal vatRate) {
		VatAmount valAmount = new VatAmount();
		valAmount.setAmount(amount);
		valAmount.setVatRate(vatRate);
		if (Validators.isNull(amount)) {
			return valAmount;
		}

		// Vat = (payAmt * vatRate)/100
		BigDecimal vatAmt = multiply(amount, vatRate);
		vatAmt = divide(vatAmt, Values.ONE_HUNDRED);

		// Include vatAmt = (payAmt + vatAmt)
		BigDecimal incVatAmt = add(amount, vatAmt);

		// Exclude vatAmt = Amount
		BigDecimal excVatAmt = amount;

		valAmount.setVatAmt(vatAmt);
		valAmount.setVatIncludeAmt(incVatAmt);
		valAmount.setVatExcludeAmt(excVatAmt);

		return valAmount;
	}

	/**
	 * The class holding calculation of VAT
	 * @author Supot Sealao
	 */
	public static class VatAmount {
		private BigDecimal amount;
		private BigDecimal vatRate;
		private BigDecimal vatAmt;
		private BigDecimal vatIncludeAmt;
		private BigDecimal vatExcludeAmt;

		public BigDecimal getAmount() {
			return amount;
		}

		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}

		public BigDecimal getVatRate() {
			return vatRate;
		}

		public void setVatRate(BigDecimal vatRate) {
			this.vatRate = vatRate;
		}

		public BigDecimal getVatAmt() {
			if (vatAmt == null) {
				vatAmt = BigDecimal.ZERO;
			}
			return vatAmt;
		}

		public void setVatAmt(BigDecimal vatAmt) {
			this.vatAmt = vatAmt;
		}

		public BigDecimal getVatIncludeAmt() {
			if (vatIncludeAmt == null) {
				vatIncludeAmt = BigDecimal.ZERO;
			}
			return vatIncludeAmt;
		}

		public void setVatIncludeAmt(BigDecimal vatIncludeAmt) {
			this.vatIncludeAmt = vatIncludeAmt;
		}

		public BigDecimal getVatExcludeAmt() {
			if (vatExcludeAmt == null) {
				vatExcludeAmt = BigDecimal.ZERO;
			}
			return vatExcludeAmt;
		}

		public void setVatExcludeAmt(BigDecimal vatExcludeAmt) {
			this.vatExcludeAmt = vatExcludeAmt;
		}

		@Override
		public String toString() {
			return "VatAmount [amount=" + amount + ", vatRate=" + vatRate + ", vatAmt=" + vatAmt + ", vatIncludeAmt="
					+ vatIncludeAmt + ", vatExcludeAmt=" + vatExcludeAmt + "]";
		}

	}
}
