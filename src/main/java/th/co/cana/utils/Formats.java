/*  ---------------------------------------------------------------------------
 *  * Copyright 2020-2021 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  ---------------------------------------------------------------------------
 */
package th.co.cana.utils; 

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * @author supot
 * @version 1.0
 */
public final class Formats {
	private static final String BYTE = " byte";
	private static final String KB = " KB";
	private static final String MB = " MB";
	private static final String GB = " GB";
	private static final String TB = " TB";
	private static final String PB = " PB";
	private static final String EB = " EB";
	private static final String ZB = " ZB";
	
	private static final String FILE_ZERO_SIZE = "0.00 byte";
	private static final DecimalFormat DFM = new DecimalFormat("#,##0.00");
	private static final DecimalFormat DF_NO_RD = new DecimalFormat("#,##0.00");
	
	static {
		DF_NO_RD.setRoundingMode(RoundingMode.FLOOR);
	}
	
	private Formats() {}

	public static String formatWithoutRound(Number value) {
		try {
			if (value == null) {
				return "";
			}
			
			double data = value.doubleValue();
			if (Math.abs(data) == data) {
				return DF_NO_RD.format(data);
			} else {
				data *= -1;
				return "-" + DF_NO_RD.format(data);
			}
		} catch (Exception ex) {
			return "";
		}
	}
	
	public static String format(Number value) {
		try {
			if (value == null) {
				return "";
			}			
			return DFM.format(value.doubleValue());
		} catch (Exception ex) {
			return "";
		}
	}
	
	public static String formatXX(String value) {
		try {
			if (value == null || value.isEmpty()) {
				return "";
			}
			value = value.replace(",", "");
			double data = Double.parseDouble(value);
			if (Math.abs(data) == data) {
				return DF_NO_RD.format(data);
			} else {
				data *= -1;
				return "-" + DF_NO_RD.format(data);
			}
		} catch (Exception ex) {
			return "";
		}
	}
}
