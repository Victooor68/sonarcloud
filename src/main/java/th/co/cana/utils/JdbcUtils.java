/*  ---------------------------------------------------------------------------
 *  * Copyright 2020-2021 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  ---------------------------------------------------------------------------
 */
package th.co.cana.utils;

import java.io.IOException;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Utility class for manage JDBC
 * 
 * @author Supot Saelao
 * @version 1.0
 */
public final class JdbcUtils {
	private static final String SQL_INSERT = "INSERT";
	private static final String SQL_UPDATE = "UPDATE";
	private static final String SQL_DELETE = "DELETE";
	private static final String SQL_SELECT = "SELECT";
	private static final String SQL_WHERE = "WHERE";
	private static final String SQL_AND = " AND ";
	private static final String COL_NAME = "COLUMN_NAME";

	private static final String FM_TIME = "HH:mm:ss";
	private static final String FM_DATE_TIME = "dd/MM/yyyy HH:mm:ss";
	private static final Character ESC_CHAR = '!';
	private static final String[] ESC_CHARS = { "!", "_", "%" };
	private static final String ERR_COL_NULL = "The columns cannot be null.";
	private static final String ERR_TB_NULL = "The table name cannot be null.";
	
	private JdbcUtils() {
	}

	/**
	 * Dynamic generate SQL INSERT prepare statement <br/>
	 * Example SQL return : <br/>
	 * <code>
	 * INSERT INTO TB(COL_1, COL_2) VALUES(?, ?)
	 * </code>
	 * 
	 * @param table   The TABLE name for generate INSERT SQL statement
	 * @param columns The column's of table
	 * @return SQL INSERT statement
	 */
	public static String createInsertSql(String table, Collection<String> columns) {
		if (Validators.isEmpty(columns)) {
			throw new IllegalArgumentException(ERR_COL_NULL);
		}

		return createInsertSql(table, columns.toArray(new String[columns.size()]));
	}

	/**
	 * Dynamic generate SQL INSERT prepare statement <br/>
	 * Example SQL return : <br/>
	 * <code>
	 * INSERT INTO TB(COL_1, COL_2) VALUES(?, ?)
	 * </code>
	 * 
	 * @param table   The TABLE name for generate INSERT SQL statement
	 * @param columns The column's of table
	 * @return SQL INSERT statement
	 */
	public static String createInsertSql(String table, String... columns) {
		if (Validators.isEmpty(table)) {
			throw new IllegalArgumentException(ERR_TB_NULL);
		}
		if (Validators.isEmpty(columns)) {
			throw new IllegalArgumentException(ERR_COL_NULL);
		}

		StringBuilder sql = new StringBuilder(512);
		StringBuilder values = new StringBuilder(64);
		sql.append("INSERT INTO ");
		sql.append(table);
		sql.append("(");

		boolean first = true;
		for (String col : columns) {
			if (first) {
				sql.append(col);
				values.append("?");
				first = false;
			} else {
				sql.append(", ");
				sql.append(col);
				values.append(", ?");
			}
		}
		sql.append(") VALUES (");
		sql.append(values.toString());
		sql.append(")");

		return sql.toString();
	}

	/**
	 * Dynamic generate SQL INSERT prepare statement with name parameter<br/>
	 * Example SQL return : <br/>
	 * <code>
	 * INSERT INTO TB(COL_1, COL_2) VALUES(:P_1, :P_2)
	 * </code>
	 * 
	 * @param table   The TABLE name for generate INSERT SQL statement
	 * @param columns The column's of table must be <br/>
	 *                <code>
	 * Map<String, String> cols = new HashMap<>();<br/>
	 * cols.put("COLUMN_NAME", "PARAMETER_NAME");
	 * </code>
	 * @return SQL INSERT statement
	 */
	public static String createInsertSql(String table, Map<String, String> columns) {
		if (Validators.isEmpty(table)) {
			throw new IllegalArgumentException(ERR_TB_NULL);
		}
		if (Validators.isEmpty(columns)) {
			throw new IllegalArgumentException(ERR_COL_NULL);
		}

		StringBuilder sql = new StringBuilder(512);
		StringBuilder values = new StringBuilder(64);
		sql.append("INSERT INTO ");
		sql.append(table);
		sql.append("(");

		boolean first = true;
		for (Map.Entry<String, String> map : columns.entrySet()) {
			String paramName = map.getValue();
			if (paramName.indexOf(":") != 0) {
				paramName = ":" + paramName;
			}
			if (first) {
				sql.append(map.getKey());
				values.append(paramName);
				first = false;
			} else {
				sql.append(", ");
				sql.append(map.getValue());
				values.append(", ");
				values.append(paramName);
			}
		}
		sql.append(") VALUES (");
		sql.append(values.toString());
		sql.append(")");

		return sql.toString();
	}

	/**
	 * Dynamic generate SQL UPDATE prepare statement <br/>
	 * Example SQL return : <br/>
	 * <code>
	 * UPDATE TB SET COL_1 = ?, COL_2 = ? WHERE PK = ?
	 * </code>
	 * 
	 * @param table   The TABLE name for generate SQL statement
	 * @param columns The column's of table
	 * @param colKey  The table WHERE column name<br/>
	 *                <string>Beware when WHERE column is null update statement not
	 *                include WHERE condition<string>
	 * @return SQL UPDATE statement
	 */
	public static String createUpdateSql(String table, Collection<String> columns, String colKey) {
		if (Validators.isEmpty(columns)) {
			throw new IllegalArgumentException(ERR_COL_NULL);
		}

		return createUpdateSql(table, colKey, columns.toArray(new String[columns.size()]));
	}

	/**
	 * Dynamic generate SQL UPDATE prepare statement <br/>
	 * Example SQL return : <br/>
	 * <code>
	 * UPDATE TB SET COL_1 = ?, COL_2 = ? WHERE PK = ?
	 * </code>
	 * 
	 * @param table   The TABLE name for generate SQL statement
	 * @param columns The column's of table
	 * @param colKey  The table WHERE column name<br/>
	 *                <string>Beware when WHERE column is null update statement not
	 *                include WHERE condition<string>
	 * @return SQL UPDATE statement
	 */
	public static String createUpdateSql(String table, String colKey, String... columns) {
		if (Validators.isEmpty(table)) {
			throw new IllegalArgumentException(ERR_TB_NULL);
		}
		if (Validators.isEmpty(columns)) {
			throw new IllegalArgumentException(ERR_COL_NULL);
		}

		StringBuilder sql = new StringBuilder(512);
		sql.append("UPDATE ");
		sql.append(table);
		sql.append(" SET ");

		boolean first = true;
		for (String col : columns) {
			if (colKey.equalsIgnoreCase(col)) {
				continue;
			}

			if (first) {
				sql.append(col);
				sql.append(" = ?");
				first = false;
			} else {
				sql.append(", ");
				sql.append(col);
				sql.append(" = ?");
			}
		}

		if (Validators.isNotEmpty(colKey)) {
			sql.append(" ").append(SQL_WHERE).append(" ");
			sql.append(colKey);
			sql.append(" = ?");
		}

		return sql.toString();
	}

	public static String createUpdateSql(String table, String[] columns, Map<String, String> colKeys, String skipKey) {

		if (Validators.isNull(colKeys)) {
			colKeys = new HashMap<>(0);
		}

		StringBuilder sql = new StringBuilder(512);
		sql.append("UPDATE ");
		sql.append(table);
		sql.append(" SET ");

		boolean first = true;
		for (String col : columns) {
			if (colKeys.containsKey(col)) {
				continue;
			}

			if (first) {
				sql.append(col);
				sql.append(" = ?");
				first = false;
			} else {
				sql.append(", ");
				sql.append(col);
				sql.append(" = ?");
			}
		}

		sql.append(" ").append(SQL_WHERE).append(" ");
		first = true;
		for (Map.Entry<String, String> map : colKeys.entrySet()) {
			if (skipKey.equals(map.getKey())) {
				continue;
			}

			if (first) {
				sql.append(map.getKey());
				sql.append(" = ?");
				first = false;
			} else {
				sql.append(SQL_AND);
				sql.append(map.getKey());
				sql.append(" = ?");
			}
		}

		return sql.toString();
	}

	public static String appendSqlCondition(String sql, String... colKeys) {
		if (Validators.isEmpty(sql)) {
			return null;
		}
		if (Validators.isEmpty(colKeys)) {
			return sql;
		}

		StringBuilder newSql = new StringBuilder(sql);
		if (isContainWhere(sql)) {
			for (String colKey : colKeys) {
				newSql.append(SQL_AND);
				newSql.append(colKey);
				newSql.append(" = ?");
			}
		} else {
			boolean first = true;
			for (String colKey : colKeys) {
				if (first) {
					newSql.append(" ").append(SQL_WHERE).append(" ");
					first = false;
				} else {
					newSql.append(SQL_AND);
				}
				newSql.append(colKey);
				newSql.append(" = ?");
			}
		}

		return newSql.toString();
	}

	public static String getDatabaseProductName(Connection conn) {
		try {
			return getDatabaseProductName(conn.getMetaData());
		} catch (SQLException ex) {
			return null;
		}
	}
	
	public static String getDatabaseProductName(DatabaseMetaData dbMeta) {
		try {
			return dbMeta.getDatabaseProductName();
		} catch (SQLException ex) {
			return null;
		}
	}
	
	public static boolean isMySql(Connection conn) {
		String dbProduct = getDatabaseProductName(conn);
		if (dbProduct == null) {
			return false;
		}
		return dbProduct.toLowerCase().contains("mysql");
	}
	
	public static boolean isOracle(Connection conn) {
		String dbProduct = getDatabaseProductName(conn);
		if (dbProduct == null) {
			return false;
		}		
		return dbProduct.toLowerCase().contains("oracle");
	}
	
	public static boolean isMsSql(Connection conn) {
		String dbProduct = getDatabaseProductName(conn);
		if (dbProduct == null) {
			return false;
		}		
		return dbProduct.toLowerCase().contains("sql server")
				|| dbProduct.toLowerCase().contains("microsoft sql server");
	}
	
	/**
	 * Verify that table name are present in the Database
	 * 
	 * @param dbMeta the DatabaseMetaData object
	 * @param table  The table for verify
	 * @return true when table is exists
	 */
	public static boolean existsTable(DatabaseMetaData dbMeta, String table) {
		ResultSet rs = null;
		try {
			String schema = getUserName(dbMeta, table);
			rs = dbMeta.getColumns(null, schema, getTableName(table), null);
			return rs.next();
		} catch (SQLException ex) {
			//Ignore
		} finally {
			close(rs);
		}

		return false;
	}

	/**
	 * List all SQL select statement column
	 * 
	 * @param rsMeta The ResultSetMetaData object
	 * @return Collection of SQL select statement column names
	 */
	public static List<String> getColums(ResultSetMetaData rsMeta) {
		List<String> columns = new ArrayList<>();
		try {
			int col = rsMeta.getColumnCount();
			for (int i = 1; i <= col; i++) {
				String colName = rsMeta.getColumnLabel(i);
				if (Validators.isEmpty(colName)) {
					colName = rsMeta.getColumnName(i);
				}
				columns.add(colName);
			}
		} catch (SQLException ex) {
			//Ignore
		}

		return columns;
	}

	public static List<ColumnInfo> getColumInfo(ResultSetMetaData rsMeta) {
		List<ColumnInfo> columns = new ArrayList<>();
		try {
			int col = rsMeta.getColumnCount();
			for (int i = 1; i <= col; i++) {
				String colName = rsMeta.getColumnLabel(i);
				if (Validators.isEmpty(colName)) {
					colName = rsMeta.getColumnName(i);
				}

				ColumnInfo type = new ColumnInfo(colName, rsMeta.getColumnType(i), rsMeta.getScale(i));
				type.setIndex(i);
				columns.add(type);
			}
		} catch (SQLException ex) {
			//Ignore
		}

		return columns;
	}

	public static List<String> getColums(DatabaseMetaData dbMeta, String table) {
		List<String> columns = new ArrayList<>();
		ResultSet rs = null;
		try {
			String schema = getUserName(dbMeta, table);
			rs = dbMeta.getColumns(null, schema, getTableName(table), null);
			while (rs.next()) {
				columns.add(rs.getString(COL_NAME));
			}
		} catch (SQLException ex) {
			//Ignore
		} finally {
			close(rs);
		}

		return columns;
	}

	public static Map<String, Integer> getColumWithTypes(ResultSetMetaData rsMeta) {
		Map<String, Integer> columns = new LinkedHashMap<>();
		try {
			int col = rsMeta.getColumnCount();
			for (int i = 1; i <= col; i++) {
				String colName = rsMeta.getColumnLabel(i);
				if (Validators.isEmpty(colName)) {
					colName = rsMeta.getColumnName(i);
				}
				columns.put(colName, rsMeta.getColumnType(i));
			}
		} catch (SQLException ex) {
			//Ignore
		}

		return columns;
	}

	public static Map<String, ColumnType> getColumWithSqlTypes(ResultSetMetaData rsMeta) {
		Map<String, ColumnType> columns = new LinkedHashMap<>();
		try {
			int col = rsMeta.getColumnCount();
			for (int i = 1; i <= col; i++) {
				String colName = rsMeta.getColumnLabel(i);
				if (Validators.isEmpty(colName)) {
					colName = rsMeta.getColumnName(i);
				}
				ColumnType type = new ColumnType(rsMeta.getColumnType(i), rsMeta.getScale(i));
				columns.put(colName, type);
			}
		} catch (SQLException ex) {
			//Ignore
		}

		return columns;
	}

	public static Map<String, String> getPrimaryKey(DatabaseMetaData dbMeta, String table) {
		Map<String, String> keys = new TreeMap<>();
		ResultSet rs = null;
		try {
			String schema = getUserName(dbMeta, table);
			rs = dbMeta.getPrimaryKeys(null, schema, getTableName(table));
			while (rs.next()) {
				keys.put(rs.getString(COL_NAME), rs.getString("PK_NAME"));
			}
		} catch (SQLException ex) {
			//Ignore
		} finally {
			close(rs);
		}

		return keys;
	}

	public static Map<String, Integer> getColumnType(DatabaseMetaData dbMeta, String table) {
		Map<String, Integer> columns = new LinkedHashMap<>();
		ResultSet rs = null;
		try {
			String schema = getUserName(dbMeta, table);
			rs = dbMeta.getColumns(null, schema, getTableName(table), null);
			while (rs.next()) {
				columns.put(rs.getString(COL_NAME), rs.getInt("DATA_TYPE"));
			}
		} catch (SQLException ex) {
			//Ignore
		} finally {
			close(rs);
		}

		return columns;
	}

	public static Map<String, ColumnType> getColumnSqlType(DatabaseMetaData dbMeta, String table) {
		Map<String, ColumnType> columns = new LinkedHashMap<>();
		ResultSet rs = null;
		try {
			String schema = getUserName(dbMeta, table);
			rs = dbMeta.getColumns(null, schema, getTableName(table), null);
			while (rs.next()) {
				ColumnType type = new ColumnType(rs.getInt("DATA_TYPE"), rs.getInt("DECIMAL_DIGITS"));
				columns.put(rs.getString(COL_NAME), type);
			}
		} catch (SQLException ex) {
			//Ignore
		} finally {
			close(rs);
		}

		return columns;
	}

	private static String getUserName(DatabaseMetaData dbMeta, String table) {
		try {
			if (Validators.isEmpty(table)) {
				return dbMeta.getUserName();
			}

			String[] schemas = table.split("[.]");
			if (schemas.length > 1) {
				return schemas[0];
			}
			return dbMeta.getUserName();
		} catch (SQLException ex) {
			return null;
		}
	}

	private static String getTableName(String table) {
		if (Validators.isEmpty(table)) {
			return table;
		}
		String[] schemas = table.split("[.]");
		if (schemas.length > 1) {
			return schemas[schemas.length - 1];
		} else {
			return table;
		}
	}

	/**
	 * Convert ResultSet value to JDBC Type <br/>
	 * Types.CLOB -> String<br/>
	 * Types.BLOB -> byte[] array
	 * 
	 * @param rs ResultSet Object
	 * @param colName SQL column name
	 * @param type java.sql.Types
	 * @return
	 * @throws SQLException
	 */
	public static Object getResultSetValue(ResultSet rs, String colName, int type) throws SQLException {
		Object obj = null;
		switch (type) {
		case Types.VARCHAR:
		case Types.CHAR:
		case Types.LONGVARCHAR:
		case Types.LONGNVARCHAR:
		case Types.NVARCHAR:
		case Types.NCHAR:
			obj = rs.getString(colName);
			break;
		case Types.INTEGER:
		case Types.BIGINT:
		case Types.DECIMAL:
		case Types.DOUBLE:
		case Types.FLOAT:
		case Types.NUMERIC:
			obj = rs.getObject(colName);
			break;
		case Types.DATE:
			obj = rs.getDate(colName);
			break;
		case Types.TIMESTAMP:
			obj = rs.getTimestamp(colName);
			break;
		case Types.TIME:
			obj = rs.getTime(colName);
			break;
		case Types.CLOB:
			obj = readClob(rs.getClob(colName));
			break;
		case Types.BLOB:
			obj = rs.getBytes(colName);
			break;
		default:
			obj = rs.getObject(colName);
		}
		
		return obj;
	}

	/**
	 * Convert ResultSet value to String <br/>
	 * Types.CLOB -> String<br/>
	 * Types.BLOB -> Base64 String
	 * 
	 * @param rs
	 * @param colName
	 * @param type
	 * @return
	 */
	public static String getResultValue(ResultSet rs, String colName, int type) {
		String value = null;
		Object obj = null;
		try {
			switch (type) {
			case Types.VARCHAR:
			case Types.NCHAR:
			case Types.CHAR:
			case Types.NVARCHAR:
			case Types.LONGNVARCHAR:
			case Types.LONGVARCHAR:
				value = rs.getString(colName);
				break;
			case Types.INTEGER:
			case Types.BIGINT:
			case Types.DECIMAL:
			case Types.DOUBLE:
			case Types.FLOAT:
			case Types.NUMERIC:
				obj = rs.getObject(colName);
				if (Validators.isNotNull(obj)) {
					value = obj.toString();
				}
				break;
			case Types.DATE:
				value = DateUtils.format(rs.getDate(colName), FM_DATE_TIME);
				break;
			case Types.TIMESTAMP:
				value = DateUtils.format(rs.getTimestamp(colName), FM_DATE_TIME);
				break;
			case Types.TIME:
				value = DateUtils.format(rs.getTime(colName), FM_TIME);
				break;
			case Types.CLOB:
				value = readClob(rs.getClob(colName));
				break;
			case Types.BLOB:
			case Types.BINARY:
			case Types.VARBINARY:
			case Types.LONGVARBINARY:
				value = readBlob(rs.getBytes(colName));
				break;
			default:
				obj = rs.getObject(colName);
				if (Validators.isNotNull(obj)) {
					value = obj.toString();
				}
			}
		} catch (SQLException ex) {
			//Ignore
		}
		return value;
	}

	public static String readClob(Clob clob) {
		if (Validators.isNull(clob)) {
			return null;
		}

		try {
			StringBuilder sb = new StringBuilder((int) clob.length());
			Reader r = clob.getCharacterStream();
			char[] cbuf = new char[2048];
			int n;
			while ((n = r.read(cbuf, 0, cbuf.length)) != -1) {
				sb.append(cbuf, 0, n);
			}
			return sb.toString();
		} catch (SQLException | IOException ex) {
			//Ignore
		}
		return null;
	}

	public static byte[] toByte(Blob blob) {
		if (blob == null) {
			return new byte[] {};
		}

		try {
			long length = blob.length();
			byte[] bytes = blob.getBytes(1l, (int)length);
			blob.free();
			
			return bytes;
		} catch (SQLException ex) {
			//Nothing
		}

		return new byte[] {};
	}
	
	public static String readBlob(Blob blob) {
		return readBlob(toByte(blob));
	}
	
	public static String readBlob(byte[] datas) {
		if (datas == null || datas.length == 0) {
			return "";
		}
		return Encryptions.encodeBase64String(datas);
	}

	public static Object toSqlValue(int type, String objVal) {
		Object obj = null;
		switch (type) {
		case Types.VARCHAR:
		case Types.NCHAR:
		case Types.CHAR:
		case Types.NVARCHAR:
		case Types.LONGNVARCHAR:
		case Types.LONGVARCHAR:
			obj = objVal;
			break;
		case Types.INTEGER:
		case Types.TINYINT:
		case Types.SMALLINT:
			obj = Convertors.toInteger(objVal);
			break;
		case Types.BIGINT:
			obj = Convertors.toLong(objVal);
			break;
		case Types.DOUBLE:
			obj = Convertors.toDouble(objVal);
			break;
		case Types.DECIMAL:
		case Types.NUMERIC:
		case Types.FLOAT:
			obj = Convertors.toBigDecimal(objVal);
			break;
		case Types.DATE:
			obj = DateUtils.sqlDate(objVal, FM_DATE_TIME);
			break;
		case Types.TIMESTAMP:
			obj = DateUtils.sqlTimestamp(objVal, FM_DATE_TIME);
			break;
		case Types.TIME:
			obj = DateUtils.sqlDate(objVal, FM_TIME);
			break;
		case Types.CLOB:
			obj = objVal;
			break;
		case Types.BLOB:
		case Types.BINARY:
		case Types.VARBINARY:
		case Types.LONGVARBINARY:
			obj = Encryptions.decodeBase64(objVal);
			break;
		default:
			obj = objVal;
			break;
		}

		return obj;
	}

	public static String getColumnName(ResultSetMetaData rsMeta, int columnIndex) throws SQLException {

		String name = rsMeta.getColumnLabel(columnIndex);
		if (Validators.isEmpty(name)) {
			name = rsMeta.getColumnName(columnIndex);
		}
		return name;
	}

	public static String toPropertyName(String name) {
		if (Validators.isEmpty(name)) {
			return null;
		}

		name = name.replaceAll("\\s+", "_");
		if (isValidProperty(name)) {
			return name;
		}

		StringBuilder result = new StringBuilder();
		boolean nextIsUpper = false;
		if (name.length() > 1 && name.substring(1, 2).equals("_")) {
			result.append(name.substring(0, 1).toUpperCase());
		} else {
			result.append(name.substring(0, 1).toLowerCase());
		}

		for (int i = 1; i < name.length(); i++) {
			String s = name.substring(i, i + 1);
			if (s.equals("_")) {
				nextIsUpper = true;
			} else {
				if (nextIsUpper) {
					result.append(s.toUpperCase());
					nextIsUpper = false;
				} else {
					result.append(s.toLowerCase());
				}
			}
		}
		return result.toString();
	}

	public static String toNestedPropertyName(String name) {
		if (Validators.isEmpty(name)) {
			return null;
		}
		
		String[] pros = name.split("\\.");
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String str : pros) {
			str = toPropertyName(str);
			if (Validators.isEmpty(str)) {
				continue;
			}
			
			if (first) {
				sb.append(str);
				first = false;
			} else {
				sb.append(".").append(str);
			}
		}
	
		return sb.toString();
	}
	
	private static boolean isValidProperty(String value) {
		if (value.contains("_")) {
			return false;
		}

		char[] chs = value.toCharArray();
		if (isLowerCase(chs)) {
			return true;
		}

		return isCamelCase(chs);
	}

	private static boolean isLowerCase(char[] chs) {
		for (char ch : chs) {
			if (!Character.isLowerCase(ch) && !Character.isDigit(ch)) {
				return false;
			}
		}

		return true;
	}

	private static boolean isCamelCase(char[] chs) {
		for (int i = 0; i < chs.length; i++) {
			char ch = chs[i];
			if (i == 0 && (Character.isUpperCase(ch) || Character.isDigit(ch))) {
				return false;
			}
		}

		return true;
	}

	public static Map<String, String> toPropertyName(ResultSetMetaData rsMeta) throws SQLException {

		if (Validators.isNull(rsMeta)) {
			return Collections.emptyMap();
		}

		Map<String, String> columnMaps = new LinkedHashMap<>();
		for (int colInx = 1, size = rsMeta.getColumnCount(); colInx <= size; colInx++) {
			String columnName = getColumnName(rsMeta, colInx);
			if (Validators.isNotEmpty(columnName)) {
				columnMaps.put(columnName, toPropertyName(columnName));
			}
		}

		return columnMaps;
	}

	public static String toClassName(String name) {
		String className = toPropertyName(name);
		if (className == null || className.isEmpty()) {
			return null;
		}
		className = String.valueOf(className.charAt(0)).toUpperCase() + className.substring(1, className.length());

		return className;
	}

	public static String toHeaderName(String columnName) {
		if (Validators.isEmpty(columnName)) {
			return null;
		}

		String[] datas = columnName.toLowerCase().split("_");
		if (Validators.isEmpty(datas)) {
			return null;
		}

		StringBuilder sb = new StringBuilder(64);
		for (int r = 0, rSize = datas.length; r < rSize; r++) {
			char[] chars = datas[r].toCharArray();
			for (int i = 0, size = chars.length; i < size; i++) {
				if (i == 0) {
					sb.append(String.valueOf(chars[i]).toUpperCase());
				} else {
					sb.append(chars[i]);
				}
			}
			sb.append(" ");
		}

		return sb.toString();
	}

	public static Map<String, Object> resultSetToMap(ResultSet rs, Map<String, String> columns) throws SQLException {

		Map<String, Object> results = new HashMap<>();
		for (Map.Entry<String, String> map : columns.entrySet()) {
			results.put(map.getKey(), rs.getObject(map.getKey()));
		}

		return results;
	}

	public static boolean isTypeDate(int type) {
		return (Types.DATE == type);
	}

	public static boolean isTypeDateTime(int type) {
		return (Types.TIMESTAMP == type);
	}

	public static boolean isTypeInteger(int type) {
		boolean isNumber = false;
		switch (type) {
		case Types.INTEGER:
		case Types.BIGINT:
			isNumber = true;
			break;
		default:
			break;
		}

		return isNumber;
	}

	public static boolean isTypeDecimal(int type) {
		boolean isNumber = false;
		switch (type) {
		case Types.DECIMAL:
		case Types.DOUBLE:
		case Types.FLOAT:
		case Types.NUMERIC:
			isNumber = true;
			break;
		default:
			break;
		}

		return isNumber;
	}

	public static boolean isTypeNumber(int type) {
		return isTypeInteger(type) || isTypeDecimal(type);
	}

	public static boolean isContainWhere(String sql) {
		if (Validators.isEmpty(sql)) {
			return false;
		}

		String[] datas = sql.split("\\s+");
		for (String str : datas) {
			if (Validators.isEmpty(str)) {
				continue;
			}

			if (SQL_WHERE.equalsIgnoreCase(str)) {
				return true;
			}
		}

		return false;
	}

	public static boolean isInsertSql(String sql) {
		if (Validators.isEmpty(sql)) {
			return false;
		}

		String[] datas = sql.split("\\s+");
		return SQL_INSERT.equalsIgnoreCase(datas[0]);
	}

	public static boolean isUpdateSql(String sql) {
		if (Validators.isEmpty(sql)) {
			return false;
		}

		String[] datas = sql.split("\\s+");
		return SQL_UPDATE.equalsIgnoreCase(datas[0]);
	}

	public static boolean isDeleteSql(String sql) {
		if (Validators.isEmpty(sql)) {
			return false;
		}

		String[] datas = sql.split("\\s+");
		return SQL_DELETE.equalsIgnoreCase(datas[0]);
	}

	public static boolean isSelectSql(String sql) {
		if (Validators.isEmpty(sql)) {
			return false;
		}

		String[] datas = sql.split("\\s+");
		return SQL_SELECT.equalsIgnoreCase(datas[0]);
	}

	public static void close(AutoCloseable rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception ex) {
			//Ignore
		}
	}

	public static String escapeChar(String value) {
		for (String str : ESC_CHARS) {
			value = value.replaceAll(str, "!" + str);
		}

		return value;
	}

	public static String getEscapeChar(String value) {
		if (isEscapeChar(value)) {
			return " escape \'" + ESC_CHAR + "\'";
		}
		return "";
	}

	public static boolean isEscapeChar(String value) {
		if (Validators.isEmpty(value)) {
			return false;
		}

		for (String str : ESC_CHARS) {
			if (value.contains(str)) {
				return true;
			}
		}
		return false;
	}

	public static String sqlFullLike(String value) {
		return "%" + escapeChar(value) + "%";
	}

	public static String sqlStartLike(String value) {
		return escapeChar(value) + "%";
	}

	public static String sqlEndLike(String value) {
		return "%" + escapeChar(value);
	}

	public static final class ColumnType {
		private int type;
		private int digit;

		public ColumnType() {
			super();
		}

		public ColumnType(int type, int digit) {
			super();
			this.type = type;
			this.digit = digit;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public int getDigit() {
			return digit;
		}

		public void setDigit(int digit) {
			this.digit = digit;
		}
	}

	public static final class ColumnInfo {
		private String column;
		private int index;
		private int type;
		private int digit;

		public ColumnInfo() {
			super();
		}

		public ColumnInfo(String column, int type, int digit) {
			super();
			this.column = column;
			this.type = type;
			this.digit = digit;
		}

		public String getColumn() {
			return column;
		}

		public void setColumn(String column) {
			this.column = column;
		}

		public int getIndex() {
			return index;
		}

		public void setIndex(int index) {
			this.index = index;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public int getDigit() {
			return digit;
		}

		public void setDigit(int digit) {
			this.digit = digit;
		}
	}
}
