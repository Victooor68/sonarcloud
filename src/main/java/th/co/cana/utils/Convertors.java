/*  ---------------------------------------------------------------------------
 *  * Copyright 2020-2021 the original author or authors.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      https://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  ---------------------------------------------------------------------------
 */
package th.co.cana.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
* Generic object converter. <br/>
* Concept by BalusC Ref : <a>http://balusc.omnifaces.org/2007/08/generic-object-converter.html</a>
* @author supot
* @version 1.0
* @see <a>http://balusc.omnifaces.org/2007/08/generic-object-converter.html</a>
*/
public final class Convertors {
	private static final Map<String, Method> CONVERTERS = new HashMap<>();
	private static final String SKIP_METHOD 	= "convert";
	private static final String TWO_ARG 		= "_2";
	
    static {
        Method[] methods = Convertors.class.getDeclaredMethods();
        for (Method method : methods) {
			if (Modifier.isPrivate(method.getModifiers()) 
					|| method.getParameterTypes().length == 0 || SKIP_METHOD.equals(method.getName())) {
				continue;
			}
        	
			String key = method.getParameterTypes()[0].getName() + "_" + method.getReturnType().getName();
			if (method.getParameterTypes().length == 1) {
				CONVERTERS.put(key, method);
			} else if (method.getParameterTypes().length == 2) {
				CONVERTERS.put(key + TWO_ARG, method);
			}
        }
    }
    
	private Convertors() {}
	
	public static <T> T convert(Object source, Class<T> target) {
		return convert(source, target, null);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T convert(Object source, Class<T> target, T defValue) {
		if (source == null) {
			return defValue;
		}

		//Cast able
		if (target.isAssignableFrom(source.getClass())) {
			return target.cast(source);
		}

		// Lookup the suitable converter.
		Method converter = getMethod(target, defValue);
		if (converter == null) {
			return defValue;
		}

		// Convert the value.
		try {
			Object value = null;
			if (defValue != null) {
				value = converter.invoke(target, source, defValue);
			} else {
				value = converter.invoke(target, source);
			}
			
			if (target.isPrimitive()) {
				return (T) value;
			}
			
			return target.cast(value);
		} catch (IllegalAccessException | IllegalArgumentException 
				| InvocationTargetException | ClassCastException ex) {
			//Ignore error
		}
		
		return null;
	}

	public static Boolean toBoolean(Object value) {
		return toBoolean(value, Boolean.FALSE);
	}
	
	public static Boolean toBoolean(Object value, Boolean defValue) {
		if (value instanceof String) {
			return Boolean.valueOf((String) value);
		}

		Number number = toNumber(value);
		if (number == null) {
			return defValue;
		}
		return (number.intValue() == 0 ? Boolean.TRUE : Boolean.FALSE);
	}
	
	public static Byte toByte(Object value) {
		return toByte(value, (byte) 0);
	}
	
	public static Byte toByte(Object value, Byte defValue) {
		try {
			if (value instanceof String) {
				return Byte.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.byteValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static Short toShort(Object value) {
		return toShort(value, (short) 0);
	}
	
	public static Short toShort(Object value, Short defValue) {
		try {
			if (value instanceof String) {
				return Short.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.shortValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static Integer toInteger(Object value) {
		return toInteger(value, 0);
	}
	
	public static Integer toInteger(Object value, Integer defValue) {
		try {
			if (value instanceof String) {
				return Integer.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.intValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static Long toLong(Object value) {
		return toLong(value, 0L);
	}
	
	public static Long toLong(Object value, Long defValue) {
		try {
			if (value instanceof String) {
				return Long.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.longValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static Double toDouble(Object value) {
		return toDouble(value, 0D);
	}
	
	public static Double toDouble(Object value, Double defValue) {
		try {
			if (value instanceof String) {
				return Double.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.doubleValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static Float toFloat(Object value) {
		return toFloat(value, 0F);
	}
	
	public static Float toFloat(Object value, Float defValue) {
		try {
			if (value instanceof String) {
				return Float.valueOf((String) value);
			}
			
			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			
			return number.floatValue();
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static BigInteger toBigInteger(Object value) {
		return toBigInteger(value, BigInteger.ZERO);
	}
	
	public static BigInteger toBigInteger(Object value, BigInteger defValue) {
		try {
			if (value instanceof String) {
				return new BigInteger((String) value);
			}

			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			return BigInteger.valueOf(number.longValue());
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static BigDecimal toBigDecimal(Object value) {
		return toBigDecimal(value, BigDecimal.ZERO);
	}
	
	public static BigDecimal toBigDecimal(Object value, BigDecimal defValue) {
		try {
			if (value instanceof String) {
				return new BigDecimal((String) value);
			}

			Number number = toNumber(value);
			if (number == null) {
				return defValue;
			}
			return BigDecimal.valueOf(number.doubleValue());
		} catch (NumberFormatException ex) {
			return defValue;
		}
	}
	
	public static String toString(Object value) {
		return toString(value, null);
	}
	
	public static String toString(Object value, String defValue) {
		if (value instanceof String) {
			return (String) value;
		}
		if (value instanceof byte[] || value instanceof Byte[]) {
			byte[] datas = (byte[]) value;
			return new String(datas, StandardCharsets.UTF_8);
		}
		
		return value != null ? value.toString() : defValue;
	}
	
	public static Date toDate(Object value) {
		return toDate(value, null);
	}
	
	public static Date toDate(Object value, Date defValue) {
		Date date = DateUtils.date(value);
		if (date != null) {
			return date;
		}

		return defValue;
	}
	
	public static byte[] toBytes(Object value) {
		return toBytes(value, new byte[0]);
	}

	public static byte[] toBytes(Object value, byte[] defValue) {
		if (Validators.isEmpty(value)) {
			return defValue;
		}
		try {
			if (value instanceof byte[] || value instanceof Byte[]) {
				return (byte[]) value;
			}
			
			String strValue = toString(value);
			if (strValue == null) {
				return defValue;
			}
			return strValue.getBytes(StandardCharsets.UTF_8);
		} catch (Exception ex) {
			return defValue;
		}
	}
	
	private static Number toNumber(Object value) {
		if (value instanceof Number) {
			return (Number) value;
		}
		
		return null;
	}

	private static Method getMethod(Class<?> target, Object defValue) {
		target = ClassUtils.getWrapper(target);
		String methodKey = Object.class.getName() + "_" + target.getName();
		if (defValue != null) {
			methodKey += TWO_ARG;
		}
		return CONVERTERS.get(methodKey);
	}
}
